Workshop 2. High Throughput Sequencing Application Session Solution 
===================================================================

Part 1
------

*Runtime: ~10 min*

Topics:

1. Downsampling FASTQ
2. Iterating through gzipped FASTQ
3. Identifying good reads
4. Deduplicating good reads
5. Counting total, good, and deduplicated reads

.. youtube:: XCAR8LPwXNQ

Part 2
------

*Runtime: ~10 min*

6. Trimming random nucleotides and adapter sequences
7. Writing to gzipped FASTA formatted file
8. Counting unique sequences

.. youtube:: M7zo9-_d-oA

Part 3
------

This got cut off a little at the end, but I was basically talking for no reason
at that point anyway so it doesn't matter.

9. Writing out stats to csv formatted file
10. Writing out sequence counts to csv formatted file
11. Running the script on downsampled and full dataset

*Runtime: ~10 min*

.. youtube:: iM23kgh-M5s

Code
----

:download:`process_reads.py <process_reads.py>`