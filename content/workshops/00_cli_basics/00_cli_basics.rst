Workshop 0. Basic Linux and Command Line Usage: Online Materials
================================================================

Introduction
++++++++++++

*Runtime: ~3 min*

.. youtube:: M5I6pNxVln4


Basic command line usage
++++++++++++++++++++++++

Navigating directories, listing files
-------------------------------------

Fundamental command line usage concepts and commands:

- ``pwd`` - print working directory
- ``ls`` - list files
- ``cd`` - change directory
- ``clear`` - clear screen output

*Runtime: ~16 min*

.. youtube:: MmHcOPJEjGA

Basic file operations
---------------------

Commands for working with directories:

- ``mkdir`` - make directory
- ``rm`` - remove file or directory
- ``mv`` - rename or change location of (move) file or directory

*Runtime: ~9 min*

.. youtube:: qG8qn4ZARvg

--------

Two videos covering commands for working with files:

- ``head`` - print first lines of a file
- ``tail`` - print last lines of a file
- ``find`` - search for a file in a and below a directory
- ``Ctl-C`` - interrupt a running command

*Runtime: ~9 min*

**NB:** At one point the speaker says the ``find`` command is not very useful.
I disagree!!

.. youtube:: hNb8gIHvN04

Two more useful commands:

- ``less`` - print a file to the screen page by page
- ``nano`` - very basic command line text editor

*Runtime: ~10 min*

.. youtube:: MJI4xTuxdPg

--------

I/O redirection and commands that commonly use it:

- ``>``, ``>>`` - redirect standard output (stdout) and replace/append to file
- ``2>``, ``2>>`` - redirect standard error (stderr) and replace/append to file
- ``&>``, ``&>>`` - redirect both stdout and stderr and replace/append to file
- ``>`` - standard input redirect from file
- ``grep`` - search for text patterns in file/output
- ``|`` - pipe, forward stdout from one program to another program
- ``cat`` - print file(s) to stdout
- ``ps`` - print out information on running processes

*Runtime: ~20 min*

.. youtube:: WXcvQ5F7Kh4

--------

Globbing

- ``*`` - the glob character

*Runtime: ~6 min*

.. youtube:: 6iQEKElUoI8

Workshop 0: terminal_quest
++++++++++++++++++++++++++

.. youtube:: wkmy9fFfX5c

:doc:`00_cli_basics_workshop`