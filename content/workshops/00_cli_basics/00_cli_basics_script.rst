Workshop 0 scripts
==================

Introduction
------------

Hi my name is Adam or some shit.

These are the materials for workshop zero.

In case you're wondering why we call this workshop zero and not workshop
one, in computer science, counting usually starts from zero.

If this sounds strange, think about measuring something with a ruler.

When you measure something to be half of an inch long, how many inches do you
have?

Zero of the units of inches, and five of the units of tenths of inches.

Now what if you measured something that is 1/100th of an inch long?

How many inches do you have?

Zero of the units of inches, and one of the units of hundreths of inches.

So, in this way, whenever we are counting units of distance, we start counting
from zero.

Computers very literally think in the same way.

Throughout these workshops, we will introduce concepts that might seem
strange or foreign, but are actually not as unfamiliar as you may think.

This first workshop is intended to give you familiarity and comfort with the
basics of using a command line interface.

These commands are available on linux and mac operating systems, but Windows is
a different animal entirely unless you have special software installed, so be
aware.

The videos below were made by Douglas Rumbaugh, who has no affiliation with BU
and someone I have never met, but his series on using linux is very accessible
so we decided not to reinvent the wheel.

Don't worry, there will be plenty of time for you to get tired of hearing my
voice throughout these workshops, so just sit tight.

The concepts covered in each of the videos are listed prior to the embed, so
you can decide if that is content you want to view or review.


Globbing
--------

One important concept the previous videos did not cover is the glob expression.

A glob is essentially an expression that describes a pattern of files.

Previously, Douglas used ls or head or grep in combination with a specific
file.

But what if you wanted to grep through all text files in a directory that
contains hundreds or even thousands of text files?

You certainly don't want to have to type out the name of each file on the
command line.

This is where globbing comes in.

In this example, say we have a directory with a large number of files that end
in ``.txt`` and other files that end in ``.pdf``.

To list out the file information for all of these files, you could just type ls
and see both ``.txt`` and ``.pdf`` files.

But if you wanted to *only* see the files that end in .txt, we can use the glob
symbol ``*`` combined with the part these filenames have in common.

In this case, we can type ``ls *.txt``, and only files that end in .txt will be
listed.

In the same way, we might type ``ls *.pdf`` to list only the pdf files.

The ``*`` character means match zero or more characters, regardless of which
characters they are.

The ``.txt`` or ``.pdf`` at the end constrains the glob to expand to only filenames
that end with those characters.

Star characters can be specified in any part of a filename pattern.

If we wanted to list all the filenames that contain an e in them, we could
type ``ls *e*``.

This glob means give me filenames that start with any number of characters,
including zero, followed by an e, followed by zero or more other characters.

This means that filenames like run.exe, eland, or file.txt all match, because
there is an e somewhere in the filename.

Note in the run.exe filename there are two 'e's, but that doesn't bother the
glob, because all it cares about is finding at least one e in the filename.

Globbing is incredibly powerful and important when you start dealing with
multiple files, as you will certainly do in your computational travels.

terminal_quest
--------------

aaaaaaaaaaad lib!