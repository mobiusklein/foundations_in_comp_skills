As the sophistication of bioinformatics methods rises to meet the growing
molecular data landscape, so does the the importance of writing code that
implements transparent, replicable, and demonstrably correct analysis. While
ideas on general strategies for organizing analysis code to meet these aims
have been proposed in the literature, concrete examples and use cases that
describe how a researcher might implement these strategies are informal and ad
hoc. In recent years, software tools that originated in the computer system
administration and software engineering fields have become increasingly
accessible to bioinformatics researchers, enabling them to implement code that
meets these needs. However, these tools and the skills needed to use them are
not typically included in formal bioinformatics curricula. At present, students
may only gain these skills through dedicated software engineering courses that
are general and not well suited to the specific and practical needs of a
bioinformatics resercher, by exposure to colleagues and mentors who already
possess the skills, or through individual motivation and self-instruction. To
address this, the BU Bioinformatics Program plans to offer training in specific
tools and skills that equip students to write, organize, and communicate code
that is effective, transparent, replicable, and correct.

The training consists of seven discrete but related modules:

  1. Basic to intermediate linux cluster usage
  2. Programming language-agnostic good programming practices
  3. Software unit and integration testing strategies
  4. Software version control software (git, github, bitbucket)
  5. Computational environment management tools (anaconda, docker)
  6. Computational workflow management tools (snakemake, NextFlow)
  7. Computational notebook platforms (jupyter, Rstudio)

Linux-based systems have become the de facto standard computational environment
for large-scale bioinformatic analysis. A solid foundation of skills in the use
of command line and cluster based tools is therefore of great utility to the
bioinformatics researcher. The linux cluster usage module will begin the
fundamentals of command line usage in linux and extend to intermediate tools
that facilitate efficient file and process management operations. Specific
topics and tools include command line navigation, file and directory creation
and manipulation, process management and communication, process input and
output redirection, terminal multiplexing and persistent login, shell
environment management and scripting, and general troubleshooting strategies.
Linux clusters, including those available at BU, often use batch job management
software (e.g. Sun Grid Engine) to control and distribute user processes across
cluster resources. This module will include training in the principles of
cluster usage and strategies for using available resources efficiently.

Good programming practices are key to writing understandable, bug-free code.
Stylistic aspects of programming, e.g. using descriptive variable names,
effective commenting, consistent code styling, et c. are simple principles that
can have profound implications on the output of a piece of code. These "soft"
aspects of programming are often taught in introductory level computer science
and software engineering courses, but bioinformatics students who come from a
predominantly biology background may not have encountered these principles.
We will pair training in stylistic concepts with the introduction of formalized
software testing processes, including unit and integration testing. Software
testing is also a concept covered in software engineering curricula, but the
application of formal testing frameworks to analysis code is relatively
unexplored. This module will use the python language as a platform for teaching
these principles, with side examples from other languages such as R to
illustrate general principles. The goal of the module is to provide students
with skills and strategies that help ensure the code they write is robust and
correct.

Software version control systems (VCS) have been used in software development
projects for decades. Distributed VCS platforms like git have facilitated
effective collaborative software development by teams of thousands of
programmers around the world, and free web-based platforms like github.com and
bitbucket.org have fostered an explosion in open source software projects. The
same principles that make VCS a boon to software development also apply to
bioinformatic analysis. In particular, shared git repositories hosted on
web-based web sites provide 1) effective backup of code in case of system
failure or accidental code loss, 2) collaborative development by multiple
analysts on the same project, 3) sharing of code with collaborators and the
community upon publication, 4) encouragement to use good programming practices
since the code will ultimately be subject to public scrutiny after publication,
and 4) a detailed record of all code changes that may be reverted and inspected
if issues arise. The software version control module will cover the use of git
to organize and track analysis code in repositories, and how to maintain,
collaborate, and share this code using the web-based platform bitbucket.org.

The number of published software packages is increasing in parallel with the
amount and types of biological data available. As a consequence, the dizzying
number of software packages and the diversity of computational environments has
led to challenges in replicating a particular bioinformatic analysis by others.
Therefore, in addition to making analysis code available, concisely describing
the computational environment, including the specific versions of software
package dependencies, is equally critical to replicating analysis. The anaconda
software suite has emerged in recent years as a solution to this problem, where
analysis code can be encapsulated in a well defined environment that can be
exported in a standardized format that others may then reproduce. Software
container technology, e.g. docker, provides a different solution to the
problem, where an entire operating system environment is encapsulated into an
image than can then be easily distributed. This module introduces these
concepts to students and trains them how to use anaconda to manage and record
the environments needed by their analysis code.

Bioinformatic analyses often utilize multiple tools glued together with bash
scripts into computational pipelines. Many projects involve tens to hundreds of
individual samples that must be processed in the same way to later be
aggregated into a single statistical analysis. Manually managing the processing
of hundreds of files with a multistep pipeline is onerous and error prone.
Fortunately, several free workflow management software packages have been
developed to address this challenge by tracking pipeline progress and
identifying and rerunning failed pipeline steps automatically. Students will be
introduced to two such workflow management software packages, snakemake and
nextflow, and trained in their application to bioinformatic analysis use cases.

Recently, software packages that combine code and output in a notebook-style
format have become popular platforms for performing and presenting analysis.
The jupyter and RStudio projects are two such packages commonly used for
performing, recording, and sharing analysis code. jupyter notebooks are
documents, accessed via a web browser, that interleve interactive code blocks
with textual and graphical output that can be easily shared. RStudio is a
development environment for the R programming language that enable convenient
inspection of code, data, and plots in a unified, web-based interface. This
module introduces the concept of the computational notebook and provides
training in how to deploy jupyter notebooks to perform and share analyses.

The concepts covered in these training modules are synergistic to the goal of
writing transparent, replicable, and correct code. Recording code in git
repositories published on the web encourages good programming practices,
enables easy sharing of code between collaborators, is an effective platform to
record the computational environment required by an analysis using anaconda, is
a safeguard against accidental loss of code, and, when combined with workflow
management software and deposition of source data into publicly available
repositories, facilitates replication of the results by other researchers.
These skills are of immediate usefulness to any graduate student who writes
code, and thus equips our students with knowledge that will benefit them and
their collaborators throughout their graduate studies and beyond.

# Relevant References:

Lewis, Joanna, Charles E. Breeze, Jane Charlesworth, Oliver J. Maclaren, and
Jonathan Cooper. 2016. “Where next for the Reproducibility Agenda in
Computational Biology?” BMC Systems Biology 10 (1): 52.

Wilson, Greg, D. A. Aruliah, C. Titus Brown, Neil P. Chue Hong, Matt Davis,
Richard T. Guy, Steven H. D. Haddock, et al. 2014. “Best Practices for
Scientific Computing.” PLoS Biology 12 (1): e1001745.

Loman, Nick, and Mick Watson. 2013. “So You Want to Be a Computational
Biologist?” Nature Biotechnology 31 (11): 996–98.

Gentleman, Robert. 2005. “Reproducible Research: A Bioinformatics Case Study.”
Statistical Applications in Genetics and Molecular Biology 4 (January):
Article2.o

Sandve, Geir Kjetil, Anton Nekrutenko, James Taylor, and Eivind Hovig. 2013.
“Ten Simple Rules for Reproducible Computational Research.” PLoS Computational
Biology 9 (10): e1003285.

Piccolo, Stephen R., and Michael B. Frampton. 2016. “Tools and Techniques for
Computational Reproducibility.” GigaScience 5 (1): 30.

Tan, Tin Wee, Joo Chuan Tong, Asif M. Khan, Mark de Silva, Kuan Siong Lim, and
Shoba Ranganathan. 2010. “Advancing Standards for Bioinformatics Activities:
Persistence, Reproducibility, Disambiguation and Minimum Information About a
Bioinformatics Investigation (MIABi).” BMC Genomics 11 Suppl 4 (December): S27.
