Introduction
============

Some friendly introductory message?

Ever since biology became a science in the 1950s, the field has moved steadily
toward being fundamentally quantitative.
It's not a coincidence that our ability to quantify biological systems tracked
with the power and sophistication of our computational systems.
When the human genome project was launched in 1990, the state of the art
consumer computer had a single core running at 25 million cycles per second,
had 8 Mb of RAM, and cost around four thousand dollars.
Today, in 2017, the state of the art mobile phone, which might be in your
pocket, has eight cores each running at around 2 billion cycles per second,
with 4000 Mb of RAM, and costs less than $1000.
The tasks our computers can perform are orders of magnitude greater than they
were even just ten years ago, and our computers have become immensely more
complex.

Getting computers to do what we want has also become more complex.
Scientific computing today requires an understanding of many different
components of the computer, from coding in multiple programming languages to
efficient memory use to managing different types of storage and so on.
It is no longer reasonable to expect a biologist, or any other person who
needs to use computers to do more than check email, to be able to learn all
of the necessary skills on their own.
It is not just a matter of not having enough time to learn; the results we
produce with our code will ideally inform real world decisions about human
health, the environment, pharmaceuticals, and other important problems.
We need to not only be *able* to wield computers to answer these types of
questions, but we must also be as sure as we can that the results are, in fact,
*correct*.
It turns out that there are no established best practices for addressing these
challenges in bioinformatics to date.
The BU Bioinformatics Program has created this series of workshops in an
attempt to give students exposure to the types of tools and techniques that
will enable them to acheive these goals.

This is the introductory video in the Foundations in Computational Skills
workshops offered by the BU Bioinformatics Program.
The series of workshops is designed to introduce core computational concepts
commonly used in bioinformatics.
We will focus on building practical skills through both guided exercises and
hands-on, real world applications.
The workshops are implemented in the so-called "flipped classroom" model,
where materials and videos explaining core concepts should be reviewed prior
to the in-class periods.
Class time is dedicated to hands-on activities related to the content, where
you are invited to work together with the help of the instructors.
There are nine workshops in total, organized into three modules of three
workshops each.

